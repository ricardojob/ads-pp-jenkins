package edu.ifpb.pp;

/**
 * 
 * @author Ricardo Job
 */
public class Calculadora {

    public int somar(Integer a, Integer b){
//            throws OperacaoMatematicaException {
        if (a == null) {
//            throw new IllegalArgumentException("Valores nulos ");
            throw new OperacaoMatematicaException("Valores nulos ");
        }
        if (b == null) {
//            throw new IllegalArgumentException("Valores nulos ");
            throw new OperacaoMatematicaException("Valores nulos ");
        }

        return a + b;
    }
    public int subtrair(Integer a, Integer b){
//            throws OperacaoMatematicaException {
         if (a == null) {
//            throw new IllegalArgumentException("Valores nulos ");
            throw new OperacaoMatematicaException("Valores nulos ");
        }
        if (b == null) {
//            throw new IllegalArgumentException("Valores nulos ");
            throw new OperacaoMatematicaException("Valores nulos ");
        }

        return a - b;
    }
    
     
}
