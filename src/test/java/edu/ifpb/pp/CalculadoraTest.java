package edu.ifpb.pp;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Job
 */
public class CalculadoraTest {

    private Calculadora calculadora;

    public CalculadoraTest() {
        calculadora = new Calculadora();
    }

    @Test
    public void somarValoresPositivos() {
        assertEquals("Valores iguais", 5, calculadora.somar(3, 2));
        assertFalse("Valores diferentes", 5 == calculadora.somar(2, 2));
        assertFalse(9 == calculadora.somar(7, 8));

    }

    @Test
    public void somarValoresNegativos() {
        assertEquals(-1, calculadora.somar(-3, 2));
        assertFalse(5 == calculadora.somar(-2, -2));
        assertFalse(9 == calculadora.somar(7, -8));

    }

    @Test(expected = OperacaoMatematicaException.class)
    public void somarValoresNulos() {
        assertEquals(0, calculadora.somar(null, 2));
        assertFalse(5 == calculadora.somar(-2, null));
        assertFalse(9 == calculadora.somar(null, null));

    }

    @Test
    public void subtrairValoresPositivos() {
        assertEquals(1, calculadora.subtrair(3, 2));
        assertFalse(5 == calculadora.subtrair(2, 2));
        assertFalse(9 == calculadora.subtrair(7, 8));

    }

    @Test
    public void subtrairValoresNegativos() {
        assertEquals(-5, calculadora.subtrair(-3, 2));
        assertFalse(5 == calculadora.subtrair(-2, -2));
        assertFalse(9 == calculadora.subtrair(7, -8));

    }

    @Test
    public void subtrairValoresCrescentes() {
        assertEquals(-6, calculadora.subtrair(2, 8));
        assertEquals(-5, calculadora.subtrair(5, 10));
        assertEquals(-4, calculadora.subtrair(-2, 2));
        assertFalse(5 == calculadora.subtrair(-2, 2));
        assertFalse(9 == calculadora.subtrair(7, -8));

    }

    @Test(expected = OperacaoMatematicaException.class)
    public void subtrairValoresNulos() {
        assertEquals(0, calculadora.subtrair(null, 2));
        assertFalse(5 == calculadora.subtrair(-2, null));
        assertFalse(9 == calculadora.subtrair(null, null));

    }
}
